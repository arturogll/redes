import sys

def exec(args,cat):
    if(args[1] == '-h' or args[1] == '--help'):
        print(help)
        return 0
    if(args[1] != '-ip'):
        print("el comando esta mal")
        return 1
    if(cat<0):
        print("la ip es mayor a 4 octetos")
        return 1
    #print("subneteo en",args[4])
    #print(".255" * (cat-1))
    sub=(cat*8)+bin(int(args[4]))
    n=toBin(bin(int(args[4])))
    for i in range(0,256,n+1):
         print("--------------------------------")
         print(args[2]+"."+str(i)+".255"*(cat-1)+"/"+str(sub))
         print(args[2]+"."+str(i+1)+".255"*(cat-1)+"/"+str(sub))
         print(args[2]+"."+str(i+n-1)+".255"*(cat-1)+"/"+str(sub))
         print(args[2]+"."+str(i+n)+".255"*(cat-1)+"/"+str(sub))   
def bin(n):
    for i in range(1,8):
        if(i**2 >= n):
            return i
def toBin(n):
    cont=0
    for i in reversed(range(8-n,8)):
        cont=cont+(2**i)
    return 255-cont
    

help = """ip subneteo


Usage:
  red.py [ip base] -n [number]
  red.py -h | --help

Example
  red.py 132 -n 5
  red.py 132.248 -n 3
  red.py 132.248.0 -n 5

Options:
  -h --help     Show this screen.
"""


args=sys.argv
cat=4-len(args[2].split('.'))

exec(args,cat)